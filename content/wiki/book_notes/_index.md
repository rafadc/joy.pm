+++
layout = "single"
title = "Book notes"
url = "/wiki/book_notes"
+++


These are the notes for the books I'm reading or have already read

- [Deep learning with python](deep_learning_with_python/)
- [Building Ethereum DApps](building_ethereum_dapps/)
