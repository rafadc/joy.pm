+++
layout = "single"
title = "Welcome to my wiki!"
url = "/wiki"
+++

I wanted to have a public archive of notes in the internet. I prefer this to be publicly accessible and I didn't want it to follow the blog post format.

- [Book notes](book_notes/)
