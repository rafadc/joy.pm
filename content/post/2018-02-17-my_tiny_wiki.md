+++
title = "My tiny wiki"
date = "2018-02-17"
+++

I'm writing some content these days that doesn't fit into the blog format. I'm experimenting with writing a small wiki in this same site. The wiki will be posted to [this same site](https://joy.pm/wiki/).

At this moment I am just adding my notes from reading books. Let's see how far we can get with this.
