# The joy of programming

My tiny blog. Based on Hugo.

## Development

```
hugo serve
```

## Deploy

Just push to source repository

``` shell
git push origin master
```
